<?php

require_once '../vendor/autoload.php';

$logger = new \Parser\SimpleLogger(__DIR__ . '/../logs/app.log');

$parser = new \Parser\Parser();
$date = new \DateTime(); //можно, например, получать дату из аргументов команды
$url = \Parser\CBHelper::generateArchiveUrl($date);

$parser
    ->setLogger($logger)
    ->getArchive($url)
    ->loadStats()
    ->saveStatsToFile(__DIR__ . '/../temp/stats_' . $date->format('d-m-Y-H:i:s') . '.xlsx')
    ->clear()
;
