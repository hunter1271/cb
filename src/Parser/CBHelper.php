<?php

namespace Parser;

class CBHelper
{
    const URL_TEMPLATE = 'http://www.cbr.ru/mcirabis/BIK/bik_db_%s.zip';

    /**
     * @param \DateTime|null $date
     *
     * @return string
     */
    public static function generateArchiveUrl(\DateTime $date = null)
    {
        if (null === $date) {
            $date = new \DateTime();
        }

        return sprintf(self::URL_TEMPLATE, $date->format('dmY'));
    }
}
