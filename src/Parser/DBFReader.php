<?php

namespace Parser;

class DBFReader implements \Iterator
{
    private $dataFile;
    private $rowsCount = 0;
    private $currentRow = 0;

    /**
     * @param $source
     */
    public function __construct($source)
    {
        if (false === function_exists('dbase_open')) {
            throw new \RuntimeException('dBase extension not installed.');
        }

        if (false === $this->dataFile = dbase_open($source, 0)) {
            throw new \RuntimeException('Invalid dBase file "'.$source.'".');
        }

        $this->rowsCount = dbase_numrecords($this->dataFile);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $result = [];

        for ($i = 1; $i <= $this->rowsCount; $i++) {
            $result[] = dbase_get_record_with_names($this->dataFile, $i);
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getRowsCount()
    {
        return $this->rowsCount;
    }

    /**
     * {@inheritdoc}
     *
     * @return array|false
     */
    public function current()
    {
        if ($this->valid()) {
            return dbase_get_record_with_names($this->dataFile, $this->currentRow);
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        $this->currentRow++;
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->currentRow;
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->currentRow <= $this->rowsCount;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->currentRow = 1;
    }

    public function __destruct()
    {
        dbase_close($this->dataFile);
    }
}