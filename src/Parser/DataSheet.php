<?php

namespace Parser;

class DataSheet
{
    private $excelObject;
    private $currentRow = 1;

    public function __construct()
    {
        $this->clear();
    }

    /**
     * @param string $newNum
     * @param string $nameP
     */
    public function add($newNum, $nameP)
    {
        $this->excelObject->getActiveSheet()
            ->setCellValue('A' . $this->currentRow, $newNum)
            ->setCellValue('B' . $this->currentRow, $nameP);

        $this->currentRow++;
    }

    /**
     * @param string $destination
     * @param null   $title
     *
     * @throws \PHPExcel_Reader_Exception
     */
    public function saveToFile($destination, $title = null)
    {
        if (null !== $title) {
            $this->excelObject->getProperties()->setTitle($title);
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($this->excelObject, 'Excel2007');
        $objWriter->save($destination);
    }

    /**
     * @throws \PHPExcel_Exception
     */
    public function clear()
    {
        $this->excelObject = new \PHPExcel();
        $this->excelObject->setActiveSheetIndex(0);
    }
}