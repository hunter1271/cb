<?php

namespace Parser;

class Downloader
{
    private $tempDir;

    /**
     * @param string $tempDir
     */
    public function __construct($tempDir)
    {
        if (!is_dir($tempDir) || !is_writeable($tempDir)) {
            throw new \RuntimeException(sprintf("'%s' is not writable dir.", $tempDir));
        }

        $this->tempDir = $tempDir;
    }

    /**
     * @param string $url
     *
     * @return string
     */
    public function download($url)
    {
        $target = $this->getTargetFilename();
        $fp = fopen($target, 'w');

        if (false === $channel = curl_init($url)) {
            fclose($fp);

            throw new \RuntimeException('Could not connect to ' . $url . '.');
        }

        curl_setopt($channel, CURLOPT_FILE, $fp);
        curl_exec($channel);
        curl_close($channel);

        fclose($fp);

        return $target;
    }

    /**
     * @return string
     */
    private function getTargetFilename()
    {
        return tempnam($this->tempDir, 'from_cb_');
    }
}
