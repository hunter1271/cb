<?php

namespace Parser;

interface LoggerInterface
{
    /**
     * @param string $message
     */
    public function info($message);

    /**
     * @param string $message
     */
    public function error($message);
}