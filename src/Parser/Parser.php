<?php

namespace Parser;

class Parser
{
    const URL_TEMPLATE = 'http://www.cbr.ru/mcirabis/BIK/bik_db_%s.zip';
    const TABLE_FILENAME = 'bnkseek.dbf';

    private $tempDir;
    private $downloader;

    private $archive;
    private $extractedDir;
    private $extractedTable;
    private $dataSheet;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string|null $tempDir
     */
    public function __construct($tempDir = null)
    {
        if (null === $tempDir) {
            $this->tempDir = __DIR__ . '/../../temp/';
        }
        $this->checkTempDir($this->tempDir);

        $this->downloader = new Downloader($this->tempDir);
        $this->dataSheet = new DataSheet();
    }

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        @set_exception_handler([$this, 'handleException']);
        @set_error_handler([$this, 'handleError']);

        return $this;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function getArchive($url)
    {
        $this->archive = $this->downloader->download($url);
        $this->logInfo('File downloaded to ' . $this->archive);
        $this->extractedDir = $this->createDestinationDir();
        $this->extractZipArchive($this->archive, $this->extractedDir);
        $this->logInfo('Files extracted to ' . $this->extractedDir);
        $this->extractedTable = $this->extractedDir . DIRECTORY_SEPARATOR . self::TABLE_FILENAME;

        if (!file_exists($this->extractedTable) || !is_file($this->extractedTable)) {
            throw new \LogicException(sprintf('Data table "\%" not found.', $this->extractedTable));
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function loadStats()
    {
        if (null === $this->extractedTable) {
            new \LogicException('Data table not received.');
        }

        $reader = new DBFReader($this->extractedTable);

        foreach ($reader as $row) {
            $this->dataSheet->add(
                $this->decodeString($row['NEWNUM']),
                $this->decodeString($row['NAMEP'])
            );
        }

        return $this;
    }

    /**
     * @param string $filename
     *
     * @return $this
     */
    public function saveStatsToFile($filename)
    {
        $this->dataSheet->saveToFile($filename, 'Report');
        $this->logInfo('Stats data saved to file: ' . $filename);

        return $this;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->clearInternal();
        $this->logInfo('Parser cleared');

        return $this;
    }

    public function clearInternal()
    {
        if (file_exists($this->archive)) {
            @unlink($this->archive);
        }

        if (file_exists($this->extractedDir)) {
            @exec('rm -rf ' . $this->extractedDir);
        }

        $this->archive = null;
        $this->extractedDir = null;
        $this->extractedTable = null;
    }

    /**
     * @param $archive
     * @param $destination
     */
    private function extractZipArchive($archive, $destination)
    {
        $zip = new \ZipArchive();
        if (true === $res = $zip->open($archive)) {
            if (false === $zip->extractTo($destination)) {
                $zip->close();

                throw new \RuntimeException(sprintf('Couldn\'t extract archive "%s" to "%s".', $archive, $destination));
            }

            $zip->close();

            return;
        }

        throw new \RuntimeException('Invalid zip archive.');
    }

    /**
     * @param string $dir
     */
    private function checkTempDir($dir)
    {
        if (!file_exists($dir)) {
            @mkdir($dir, 0777, true);
        }

        if (!is_dir($dir) || !is_writeable($dir)) {
            throw new \RuntimeException(sprintf("'%s' is not writable dir.", $dir));
        }
    }

    /**
     * @return string
     */
    private function createDestinationDir()
    {
        $now = new \DateTime();
        $dir = $this->tempDir . 'extracted_' . $now->format('YmdHis');

        if (false === mkdir($dir)) {
            throw new \RuntimeException('Could\'n create directory "'.$dir.'".');
        }

        return $dir;
    }

    /**
     * @param string $input
     *
     * @return string
     */
    private function decodeString($input)
    {
        $input = iconv("CP866", "UTF-8", $input);

        return trim($input);
    }

    /**
     * @param string $message
     */
    private function logInfo($message)
    {
        if ($this->logger) {
            $this->logger->info($message);
        }
    }

    /**
     * @param \Exception $ex
     *
     * @throws \Exception
     */
    public function handleException(\Exception $ex)
    {
        if ($this->logger) {
            $this->logger->error(sprintf('[%s] %s', get_class($ex), $ex->getMessage()));
        }
        $this->clearInternal();

        throw $ex;
    }

    /**
     * @param int    $errNo
     * @param string $errMsg
     *
     * @return bool
     */
    public function handleError($errNo, $errMsg)
    {
        if ($this->logger) {
            $this->logger->error(sprintf('[Error level: %s] %s', $errNo, $errMsg));
        }
        $this->clearInternal();

        return false;
    }
}
