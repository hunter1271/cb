<?php

namespace Parser;

class SimpleLogger implements LoggerInterface
{
    const LEVEL_INFO = 'INFO';
    const LEVEL_ERROR = 'ERROR';

    private $logFile;
    private $handler;

    /**
     * @param string $logFile
     */
    public function __construct($logFile)
    {
        $dir = dirname($logFile);

        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if (false === $this->handler = fopen($logFile, 'a')) {
            throw new \RuntimeException(sprintf('Log file \'%s\' is not writable.', $logFile));
        }
        $this->logFile = $logFile;
    }

    /**
     * @param string $message
     */
    public function info($message)
    {
        $this->writeLog(self::LEVEL_INFO, $message);
    }

    /**
     * @param string $message
     */
    public function error($message)
    {
        $this->writeLog(self::LEVEL_ERROR, $message);
    }

    /**
     * @param string $level
     * @param string $message
     */
    private function writeLog($level, $message)
    {
        @fwrite($this->handler, sprintf("[%s] %s: %s\n", date('Y-m-d H:i:s'), $level, $message));
    }

    public function __destruct()
    {
        fclose($this->handler);
    }
}